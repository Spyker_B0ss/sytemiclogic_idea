app = {
    api: {
        create: function() {
            var file_data = $('#idea_image').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('title', $('#title').val());
            form_data.append('description', $('#description').val());
            form_data.append('email', $('#email').val());
            form_data.append('category', $('#category').val());
            form_data.append('terms', $('#terms').is('checked'));
            form_data.append('private', $('input[name=privateIdea]:checked').val());

            var url = "api/create.php";
            $.ajax({
                url: url, // point to server-side PHP script
                dataType: 'json', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response) {
                    $('form').addClass('hide');
                    $('.success').append(JSON.stringify(response))
                },
                error: function(response) {
                    returnData = response;
                }
            });

        },
        read: {
            all: function() {
                var url = "api/read.php";
                $.ajax({
                    url: url, // point to server-side PHP script
                    dataType: 'json', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'get',
                    success: function(response) {
                        var itemHtml = '';
                        if (response.length > 0) {
                            for (i = 0; i < response.length; i++) {
                                itemHtml += '<div class="panel panel-primary">';
                                itemHtml += '<div class="panel-heading">' + response[i].title + '</div>';
                                itemHtml += '<div class="panel-body">' + response[i].description + '</div>';
                                itemHtml += '<div class="panel-footer"><a href="update.html?id=' + response[i].id + '" class="update"><i class="fa fa-pencil-square-o"></i><span>Update</span></a><a href="#" data-id="' + response[i].id + '" class="delete"><i class="fa fa-trash-o"></i><span>Delte</span></a></div></div>';
                            }

                        } else {
                            itemHtml += '<div class="panel panel-danger">';
                            itemHtml += '<div class="panel-heading">Oops!</div>';
                            itemHtml += '<div class="panel-body"><a href="create.html">There are no ideas yet, be the first?</a></div>'
                            itemHtml += '</div>';
                        }
                        $('.ideas_container').append(itemHtml);

                        $('.delete').click(function(e) {
                            e.preventDefault();

                            var form_data = new FormData();
                            form_data.append('id', $(this).data('id'));
                            app.api.delete(form_data, $(this).parent().parent());
                        });



                    },
                    error: function() {

                    }
                });
            },
            single: function(form_data, callback) {
                var url = "api/read.php";
                $.ajax({
                    url: url, // point to server-side PHP script
                    dataType: 'json', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'POST',
                    success: function(response) {
                        if (response.length > 0) {
                            callback(response);
                        } else {

                        }
                    },
                    error: function() {

                    }
                });

            }
        },
        delete: function(form_data, parentElement) {
            var url = "api/delete.php";
            $.ajax({
                url: url, // point to server-side PHP script
                dataType: 'json', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response) {
                    $(parentElement).remove();
                },
                error: function(response) {
                    returnData = response;
                }
            });
        },
        update: function(id) {
            var url = "api/update.php";

            var file_data = $('#idea_image').prop('files')[0];

            var form_data = new FormData();
            form_data.append('id', id);
            if($('#currentFile').attr('src').length > 0 && typeof file_data != 'undefined')
              form_data.append('file', file_data);
            else if($('#currentFile').attr('src').length > 0 && typeof file_data == 'undefined')
              form_data.append('file',$('#currentFile').attr('src'));
            else
              form_data.append('file','');

            form_data.append('title', $('#title').val());
            form_data.append('description', $('#description').val());
            form_data.append('email', $('#email').val());
            form_data.append('category', $('#category').val());
            form_data.append('terms', $('#terms').is('checked'));
            form_data.append('private', $('input[name=privateIdea]:checked').val());

            $.ajax({
                url: url, // point to server-side PHP script
                dataType: 'json', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response) {
                    $('form').addClass('hide');
                    $('.success').append(JSON.stringify(response))
                },
                error: function(response) {
                    returnData = response;
                }
            });
        }
    },
    validation: {
        isNotNullOrEmpty: function(str) {
            return !str || str.length === 0;
        },
        isValidEmail: function(emailAddress) {
            var re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
            return re.test(emailAddress);
        },
        isChecked: function(element) {
            return $(element).is(":checked")
        }
    },
    getParameterByName: function(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    create: function() {

        $(document).on('click', '.browse', function() {
            var file = $(this).parent().parent().parent().find('.file');
            file.trigger('click');
        });
        $(document).on('change', '.file', function() {
            $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
        });

        var canSubmit = false;


        $('#submit').click(function() {

            if (!$('#terms').is(':checked'))
                $('.terms-error').addClass('show');
            else
                $('.terms-error').removeClass('show');

            if ($("#category")[0].selectedIndex >= 0)
                $('.category-error').removeClass('show');
            else
                $('.category-error').addClass('show');

            if ($("#category")[0].selectedIndex >= 0)
                canSubmit = true;

            if ($("#idea_creation_form").valid() && $('#terms').is(':checked') && canSubmit)
                app.api.create()

        });
    },
    update: {
        read: function() {
            var id = app.getParameterByName('id');
            var form_data = new FormData();
            form_data.append('id', id);

            function callback(data) {

                $('#title').val(data[0].title);
                $('#description').val(data[0].description);
                $('#email').val(data[0].email);
                $('#category').val(data[0].category);
                if(data[0].imageLocation && data[0].imageLocation.length > 0)
                $('#currentFile').attr({
                    'src': data[0].imageLocation
                });

                if (data[0].private == "1")
                    $('#privateIdea').prop("checked", true);
                else
                    $('#publicIdea').prop("checked", true);

                app.update.assignEvents(data[0].id);
            }

            app.api.read.single(form_data, callback);
        },
        assignEvents: function(id) {
            $(document).on('click', '.browse', function() {
                var file = $(this).parent().parent().parent().find('.file');
                file.trigger('click');
            });
            $(document).on('change', '.file', function() {
                $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
            });

            var canSubmit = false;


            $('#submit').click(function() {

                if (!$('#terms').is(':checked'))
                    $('.terms-error').addClass('show');
                else
                    $('.terms-error').removeClass('show');

                if ($("#category")[0].selectedIndex >= 0)
                    $('.category-error').removeClass('show');
                else
                    $('.category-error').addClass('show');

                if ($("#category")[0].selectedIndex >= 0)
                    canSubmit = true;

                if ($("#idea_creation_form").valid() && $('#terms').is(':checked') && canSubmit)
                    app.api.update(id);

            });
        }
    },
    init: function() {
        $(document).ready(function() {
            var bodyEl = document.body,
                content = document.querySelector('.content-wrap'),
                openbtn = document.getElementById('open-button'),
                closebtn = document.getElementById('close-button'),
                isOpen = false;

            openbtn.addEventListener('click', function(ev) {
                var target = ev.target;
                if (!isOpen && target == openbtn) {
                    $('body').addClass('show-menu');
                }
                isOpen = !isOpen;
            });
            if (closebtn) {
                closebtn.addEventListener('click', function(ev) {
                    var target = ev.target;
                    if (isOpen && target !== openbtn) {
                        $('body').removeClass('show-menu');
                        isOpen = !isOpen;
                    }
                });
            }

            // close the menu element if the target it´s not the menu element or one of its descendants..
            content.addEventListener('click', function(ev) {
                var target = ev.target;
                if (isOpen && target !== openbtn) {
                    $('body').removeClass('show-menu');
                }
                isOpen = !isOpen;
            });


            switch (window.location.pathname) {
                case '/update.html':
                    app.update.read();
                    break;
                case '/create.html':
                    app.create();
                    break;
                default:
                    app.api.read.all();
                    break;
            }
        });
    }
};
app.init();
