<?php
    include 'db.php';

    $pdo = Database::connect();
    $jsonData = array();
    if(empty($_POST)){

      $sql = 'SELECT * FROM ideas ORDER BY ID DESC';

      foreach ($pdo->query($sql) as $row) {
        $jsonData[] = array('id'=> $row['ID'],'title' => $row['Title'],'description' => $row['Description'],'email' => $row['Email'],'category' => $row['Category'],'imageLocation' => $row['ImageLocation'],'private' => $row['Private']);
      }


    }
    else{
      $sql = 'SELECT * FROM ideas WHERE ID = ' . $_POST['id'];

      foreach($pdo->query($sql) as $row) {
        $jsonData[] = array('id'=> $row['ID'],'title' => $row['Title'],'description' => $row['Description'],'email' => $row['Email'],'category' => $row['Category'],'imageLocation' => $row['ImageLocation'],'private' => $row['Private']);
      }
    }

    Database::disconnect();
    echo json_encode($jsonData);
?>
