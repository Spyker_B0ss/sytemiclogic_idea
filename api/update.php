<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require "db.php";

 if(!empty($_POST)){

   $responsedata = array('success' => false ,'errors' => array('title' => false,'description' => false,'validemail' => false,'noemail' => false,'category' => false,'terms'=> false,'imgUpload' => false,'imgUploadMessage' => ''), 'imagePath' => '');

   $id = $_POST['id'];
   $title = $_POST['title'];
   $description = $_POST['description'];
   $useremail = $_POST['email'];
   $category = $_POST['category'];
   $terms = $_POST['terms'];
   $private = $_POST['private'];
   if(isset($_POST['file']))
   $file = $_POST['file'];
   $validform = true;

   if(empty($title))
   {
     $responsedata['errors']['title'] = true;

   }
   if (empty($useremail)) {
     $responsedata['errors']['noemail'] = true;
     $validForm = false;
  } else if ( !filter_var($useremail,FILTER_VALIDATE_EMAIL) ) {
    $responsedata['errors']['validemail'] = true;
    $validForm = false;
  }

  if (empty($description)) {
    $responsedata['errors']['description'] = true;
    $validForm = false;
  }


  if (empty($category)) {
    $responsedata['errors']['category'] = true;
    $validform = false;
  }

  if (empty($terms)) {
    $responsedata['errors']['terms'] = true;
    $validform = false;
  }

  if(isset($_FILES["file"]["type"]))
  {
    $validextensions = array("jpeg", "jpg", "png");
    $temporary = explode(".", $_FILES["file"]["name"]);
    $file_extension = end($temporary);
    if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg") ||($_FILES["file"]["type"] == "image/gif")) && ($_FILES["file"]["size"] < 2097152 ) && in_array($file_extension, $validextensions))
    {
      if ($_FILES["file"]["error"] > 0)
      {
        $responsedata['errors']['imgUpload'] = true;
        $responsedata['errors']['imgUploadMessage'] = $_FILES["file"]["error"];
      }
      else
      {
        $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
        //random number based on the current time and append the url friendly name from the originally uploaded file.
        //prevents duplicate images
        $targetPath = "ideas_images/". round(microtime(true)) . '_' . urlencode($_FILES['file']['name']);
        move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file

        $responsedata['imagePath'] = $targetPath;
        $fileLocation = $targetPath;
      }
    }
    else
    {
      $responsedata['errors']['imgUpload'] = true;
      $responsedata['errors']['imgUploadMessage'] = "Invalid type or Image is to large";
    }
  }
  else{
    $fileLocation = $file;
  }

  if ($validform) {
    $pdo = Database::connect();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "UPDATE ideas SET Title = ?, Description = ?,Email = ?,Category = ?,ImageLocation = ?,Private = ? Where ID = ?";
    $q = $pdo->prepare($sql);
    if(stripos($fileLocation, 'api/') === false){
    $fileLocation = 'api/'.$fileLocation;
    }

      $q->execute(array($title,$description,$useremail,$category,$fileLocation,$private,$id));

    Database::disconnect();
  }
  $responsedata['success'] = $validform;
  echo json_encode($responsedata);
}

?>
