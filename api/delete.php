<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require "db.php";

 if(!empty($_POST)){

   $pdo = Database::connect();
   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $sql = "DELETE FROM ideas WHERE ID = ? ";
   $q = $pdo->prepare($sql);
   $q->execute(array($_POST['id']));
   Database::disconnect();

   echo json_encode(array('success' => true));
 }
 ?>
