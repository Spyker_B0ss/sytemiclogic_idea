'use strict';

module.exports = function(grunt) {

    /* Configure
  ============================ */
    var configs = {
        css_combine_files: [

        ],
        js_combine_files: [
          'bower_components/jquery/dist/jquery.min.js',
          'bower_components/bootstrap/dist/js/bootstrap.min.js'
        ],
        watch_files: [
            'styles/*',
            'scripts/*'
        ]
    };
    /* Init
  ============================ */
    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'styles',
                    src: ['*.scss'],
                    dest: 'dist/css',
                    ext: '.css'
                }]
            }
        },
        uglify: {
            my_target: {
                files: {
                    'dist/js/compiled.min.js': configs.js_combine_files
                }
            }
        },
        watch: {
            styles: {
                files: ['styles/*.scss'], // which files to watch
                tasks: ['sass'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    // Add plugins
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Register tasks
    grunt.registerTask('default', ['sass', 'uglify', 'watch']);


    grunt.event.on('watch', function(action, filepath) {
        grunt.log.writeln(filepath + ' has ' + action);
    });

};
