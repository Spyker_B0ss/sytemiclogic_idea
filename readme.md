# Libraries
1. jQuery
2. Bootstrap

I chose to use jQuery and Bootsrap to illustrate a basic html, css and jQuery idea capturing app.

# Install

## Database
> _db\systemiclogic_ideas.sql

The SQL needed to create the Database as well as the table.

Run the script file on the MySQL server using a user with create database privileges.

Create a user in the MySQL Server with Create, Update, Delete privileges on the systemiclogic_ideas database.


## Database Connection
Update `api\db.php` line 4 - 7 with the user's details that has been created.

```
private static $dbName = 'systemiclogic_ideas' ;
private static $dbHost = '<MQSQLDB( localhost )>' ;
private static $dbUsername = '<UserName>';
private static $dbUserPassword = '<Password>';

```

## Grunt
To install the grunt libraries needed in this project simply run
```
npm install
```

# API Reference
## Create
`api/create.php`
`POST - JSON`
Create an idea in the database.

## Read
`api/read.php`

`POST - JSON`
With ID in order to read a single idea entry

`GET - JSON`
Will return all rows in the database

## Update
`api/update.php`

`POST - JSON`
With ID and field to update Idea in database

## Delete
`api/delete.php`

`POST - JSON`
With ID of the idea entry, this will remove the entry in the database.